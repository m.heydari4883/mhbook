//
//  Size+Intro.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/13/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

/*************************************************/
/*********** Extension for Controller ************/
/*************************************************/
extension Size.Controller {
    
    struct IntroSlider {
        
        // collection view
        static let collectionViewSize: CGSize = {
            let height = Size.Window.aHeight
            let width = Size.Window.aWidth
            return .init(width: width, height: height)
        }()
        
        // button 'Got it'
        static let btnGotItInset = UIEdgeInsets(top: 8, left: 18, bottom: 8, right: 24)
        static let btnGotITFont: CGFloat = 16
        static let btnGotItBottomPadding: CGFloat = -12
        
        // page control
        static let pageControlBottomPadding: CGFloat = -12

        
    }
    
}


/**************************************************/
/**************  Extension for View  **************/
/**************************************************/
extension Size.View {
    
    struct IntroCell {
        // image logo
        static let imgLogoHeight: CGFloat = 100
        static let imgLogoWidth: CGFloat = 100
        static let imgLogoTopPaddign: CGFloat = 150
        static let imgLogoCornerRadius: CGFloat = 12
        
        
        // label title
        static let txtTitleFont: CGFloat = 28
        static let lblTitleTopPaddign: CGFloat = 75
        
        // label subtitle
        static let txtSubtitleFont: CGFloat = 18
        static let lblSubtitleTopPaddign: CGFloat = 30
        static let lblSubtitleRightPaddign: CGFloat = -25
        static let lblSubtitleLeftPaddign: CGFloat = 25
        
    }
}
