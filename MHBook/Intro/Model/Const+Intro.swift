//
//  Const+Intro.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/13/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

/*************************************************/
/*********** Extension for Controller ************/
/*************************************************/
extension Const.Controller {
    
    struct IntroSlider {
        static let cellId = "intro-collection-view-controller-cell-id"
        static let btnGotItTitle = "Got It"
    }
    
}
