//
//  Model+Intro.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/13/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

/************************************************/
/************* Extension for Intro **************/
/************************************************/
extension Model.Intro {
    
    // page 1
    static let page1 = IntroPageModel(image: nil, title: "Post", subTitle: "Share posts, photos, and comments with your network.")
    // page 2
    static let page2 = IntroPageModel(image: nil, title: "Stories", subTitle: "Showe stories that disappear after 24h.")
    // page 3
    static let page3 = IntroPageModel(image: nil, title: "Check in", subTitle: "Check in when postingto sahre your location with friends.")
    // page 4
    static let page4 = IntroPageModel(image: nil, title: "Reactions", subTitle: "React to posts and photoswith likes, dislikes and more.")
    // page 5
    static let page5 = IntroPageModel(image: nil, title: "Chat", subTitle: "Commiunicate with your friends via private messages.")
    // page 6
    static let page6 = IntroPageModel(image: nil, title: "Get Notified", subTitle: "Recieve notifiacations when friends are locking for you")
    
}

func getIntroModel() -> [IntroPageModel] {
    return [
        Model.Intro.page1,
        Model.Intro.page2,
        Model.Intro.page3,
        Model.Intro.page4,
        Model.Intro.page5,
        Model.Intro.page6
    ]
}



