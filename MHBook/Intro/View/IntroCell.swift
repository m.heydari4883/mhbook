//
//  IntroCell.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/13/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

class IntroCell: UICollectionViewCell {
    
    var introModel: IntroPageModel? {
        didSet {
            guard let introModel = introModel else { return }
            configureCell(introModel: introModel)
        }
    }
    
    let imageLogo: UIImageView = {
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.constrainWidth(constant: Size.View.IntroCell.imgLogoWidth)
        img.constrainHeight(constant: Size.View.IntroCell.imgLogoHeight)
        img.layer.cornerRadius = Size.View.IntroCell.imgLogoCornerRadius
        img.backgroundColor = .white
        return img
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.numberOfLines = 1
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: Size.View.IntroCell.txtTitleFont, weight: .heavy)
        return label
    }()
    
    let subTitleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.numberOfLines = 2
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: Size.View.IntroCell.txtSubtitleFont, weight: .regular)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        backgroundColor = Color.View.IntroCell.cellbackground
        addViews()
        addViewConstraints()
    }
    
    private func addViews() {
        [imageLogo, titleLabel, subTitleLabel]
            .forEach{ addSubview($0) }    }
    
    private func addViewConstraints() {
        [imageLogo.centerXAnchor.constraint(equalTo: centerXAnchor),
         imageLogo.topAnchor.constraint(equalTo: topAnchor, constant: Size.View.IntroCell.imgLogoTopPaddign),
         
         titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
         titleLabel.topAnchor.constraint(equalTo: imageLogo.bottomAnchor, constant: Size.View.IntroCell.lblTitleTopPaddign),
         
         subTitleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: Size.View.IntroCell.lblSubtitleLeftPaddign),
         subTitleLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: Size.View.IntroCell.lblSubtitleRightPaddign),
         subTitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: Size.View.IntroCell.lblSubtitleTopPaddign),
         
         
         ].forEach{ $0.isActive = true }
    }
    
    private func configureCell(introModel: IntroPageModel) {
        self.imageLogo.image = introModel.image
        self.titleLabel.text = introModel.title
        self.subTitleLabel.text = introModel.subTitle
    }
}
