//
//  IntroSliderController+CollectionView.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/13/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

extension IntroSliderController {
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Const.Controller.IntroSlider.cellId, for: indexPath) as? IntroCell else { return UICollectionViewCell() }
        cell.introModel = pages[indexPath.item]
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return Size.Controller.IntroSlider.collectionViewSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}



