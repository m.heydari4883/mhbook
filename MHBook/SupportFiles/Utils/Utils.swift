//
//  Utils.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/15/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

func calculatedImageHeight(imgHeight: CGFloat, imgWidth: CGFloat) -> CGFloat {
    return  (imgHeight / imgWidth) * Size.Window.aWidth
}
