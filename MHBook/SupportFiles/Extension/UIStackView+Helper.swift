//
//  UIStackView+Helper.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/12/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

extension UIStackView {
    func addArrangedSubview(views: [UIView]) {
        views.forEach {addArrangedSubview($0) }
    }
    
    convenience init(axis: NSLayoutConstraint.Axis, spacing: CGFloat = 0) {
        self.init(arrangedSubviews: [])
        self.axis = axis
        self.spacing = spacing
        translatesAutoresizingMaskIntoConstraints = false
    }
}


/*
 
 
 
 class VerticalStackView: UIStackView {
 public init(subviews: [UIView], spacing: CGFloat = 0) {
 super.init(frame: .zero)
 self.spacing = spacing
 self.axis = .vertical
 subviews.forEach({addArrangedSubview($0)})
 }
 
 required init(coder: NSCoder) {
 fatalError("init(coder:) has not been implemented")
 }
 }
 
 
 extension UIStackView {
 
 convenience init(subviews: [UIView], customSpacing: CGFloat) {
 self.init(arrangedSubviews: subviews)
 self.spacing = customSpacing
 }
 }

 
 
 */
