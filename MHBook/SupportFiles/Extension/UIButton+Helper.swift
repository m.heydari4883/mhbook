//
//  UIButton+Helper.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/12/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

extension UIButton {
    func setSystemFont(ofSize: CGFloat, weight: UIFont.Weight) {
        self.titleLabel?.font = .systemFont(ofSize: ofSize, weight: weight)
    }
    
    func tappedAnimation() {
        UIView.animate(withDuration: 0.2, animations: { [weak self] in
            self?.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        }) { (_) in
            UIView.animate(withDuration: 0.2, animations: { [weak self] in
                self?.transform = .identity
            })
        }
    }
}
