//
//  snap.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/13/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

protocol SnappingCollectionViewFlowLayoutDelegate: class {
    func indexForCurrentItem(index: Int)
}


class SnappingCollectionViewFlowLayout: UICollectionViewFlowLayout {
    
    private var lastIndex: Int = -10 // this is an arbitary number exept positives and -1
    weak var delegate: SnappingCollectionViewFlowLayoutDelegate?
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        
        guard let collectionView = collectionView else {
            return super.targetContentOffset(forProposedContentOffset: proposedContentOffset, withScrollingVelocity: velocity)
        }
        
        let nextX: CGFloat
        
        if proposedContentOffset.x <= 0 || collectionView.contentOffset == proposedContentOffset {
            nextX = proposedContentOffset.x
        } else {
            nextX = collectionView.contentOffset.x + (velocity.x > 0 ? collectionView.bounds.size.width : -collectionView.bounds.size.width)
        }
        
        let targetRect = CGRect(x: nextX, y: 0, width: collectionView.bounds.size.width, height: collectionView.bounds.size.height)
        var offsetAdjustment = CGFloat.greatestFiniteMagnitude
        
        let horizontalOffset = proposedContentOffset.x + collectionView.contentInset.left
        
        let layoutAttributesArray = super.layoutAttributesForElements(in: targetRect)
        
        layoutAttributesArray?.forEach({ (layoutAttributes) in
            let itemOffset = layoutAttributes.frame.origin.x
            if fabsf(Float(itemOffset - horizontalOffset)) < fabsf(Float(offsetAdjustment)) {
                offsetAdjustment = itemOffset - horizontalOffset
            }
        })
        
        
        let deviceWidth = Size.Window.aWidth
        let currentCellXPoint = offsetAdjustment > deviceWidth ? deviceWidth : offsetAdjustment
        
        let currentCellPoint = CGPoint(x: proposedContentOffset.x + currentCellXPoint, y: proposedContentOffset.y)
        let indexPath = collectionView.indexPathForItem(at: currentCellPoint)?.item ?? -1
        
        if indexPath == -1 {
            delegate?.indexForCurrentItem(index: lastIndex)
        } else {
            delegate?.indexForCurrentItem(index: indexPath)
        }
        
        lastIndex = indexPath != -1 ? indexPath : lastIndex
        return CGPoint(x: proposedContentOffset.x + offsetAdjustment, y: proposedContentOffset.y)
    }
    
    private func getCurrentCollectionViewCellIndex(_ collectionView: UICollectionView, proposedContentOffset: CGPoint,offset: CGFloat) -> Int {
        
        return 1
    }
    
}

