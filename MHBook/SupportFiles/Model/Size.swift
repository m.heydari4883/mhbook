//
//  Size.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/12/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

struct Size {
    
    struct Window {
        /// a stands to -> 'actual' : actual width of device, independant of device orientation
        static let aWidth: CGFloat = {
            if UIApplication.shared.statusBarOrientation.isPortrait {
                // device is in portrait mode
                return UIScreen.main.bounds.width
            }
            return UIScreen.main.bounds.height
        }()
        
        /// a stands to -> 'actual' : actual height of device, independant of device orientation
        static let aHeight: CGFloat = {
            if UIApplication.shared.statusBarOrientation.isPortrait {
                // device is in portrait mode
                return UIScreen.main.bounds.height
            }
            return UIScreen.main.bounds.width
        }()
        
        static let width: CGFloat = {
            return UIScreen.main.bounds.height
        }()
        
        static let height: CGFloat = {
            return UIScreen.main.bounds.width
        }()
    }
    
    struct Controller { }
    
    struct View { }
}
