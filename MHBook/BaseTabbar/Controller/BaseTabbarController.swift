//
//  BaseTabbarController.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/13/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

class BaseTabBarController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupTabBarViewControllers()
    }
    
    private func setupView() {
        view.backgroundColor = .white
        tabBar.isTranslucent = false
    }
    
    private func setupTabBarViewControllers() {
        viewControllers = [
            generateTabNavController(with: HomeController(), title: Const.Controller.Home.navTitle, image: #imageLiteral(resourceName: "home")),
            generateTabNavController(with: DiscoverController(), title: Const.Controller.Discover.navTitle, image: #imageLiteral(resourceName: "discover")),
            generateTabNavController(with: ChatsController(), title: Const.Controller.Chats.navTitle, image: #imageLiteral(resourceName: "chat")),
            generateTabNavController(with: FriendShipController(), title: Const.Controller.FriendShip.navTitle, image: #imageLiteral(resourceName: "FriendShip")),
            generateTabNavController(with: ProfileController(), title: Const.Controller.Profile.navTitle, image: #imageLiteral(resourceName: "profile"))
        ]
    }
    
    private func generateTabNavController(with controller: UIViewController, title: String, image: UIImage) -> UIViewController {
        controller.view.backgroundColor = .white
        controller.navigationItem.title = title
        let navController = UINavigationController(rootViewController: controller)
        navController.tabBarItem.image = image
        navController.navigationBar.isTranslucent = false
        navController.navigationBar.prefersLargeTitles = false
//        navController.tabBarItem.title = title
        return navController
    }
    
}
