//
//  SignUpView.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/12/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

class SignUpView: UIView {
    
    let labelCreateNewAccount: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Color.View.LogIn.lightBlue
        label.text = Const.View.SignUp.createNewAccount
        label.textAlignment = .left
        label.font = UIFont.systemFont(ofSize: Size.View.SignUp.txtCreateNewAccountFont, weight: .heavy)
        label.numberOfLines = 1
        return label
    }()
    
    let fullNameTextField: MHTextField = {
        let padding = Size.View.SignUp.textFieldInnerPadding
        let height = Size.View.SignUp.textFieldHeight
        
        let tf = MHTextField(padding: padding, height: height)
        tf.placeholder = Const.View.SignUp.fullNamePlaceHolder
        
        tf.constrainHeight(constant: height)
        
        tf.layer.borderColor = Color.View.SignUp.textFieldBorder
        tf.layer.borderWidth = Size.View.SignUp.textFieldBorderWidth
        
        return tf
    }()
    
    let phoneNumberTextField: MHTextField = {
        let padding = Size.View.SignUp.textFieldInnerPadding
        let height = Size.View.SignUp.textFieldHeight
        
        let tf = MHTextField(padding: padding, height: height)
        tf.placeholder = Const.View.SignUp.phoneNumberPlaceHolder
        
        tf.constrainHeight(constant: height)
        
        tf.layer.borderColor = Color.View.SignUp.textFieldBorder
        tf.layer.borderWidth = Size.View.SignUp.textFieldBorderWidth
        
        return tf
    }()
    
    let emailTextField: MHTextField = {
        let padding = Size.View.SignUp.textFieldInnerPadding
        let height = Size.View.SignUp.textFieldHeight
        
        let tf = MHTextField(padding: padding, height: height)
        tf.placeholder = Const.View.SignUp.emailAddressPlaceHolder
        tf.keyboardType = .emailAddress
        tf.constrainHeight(constant: height)
        
        tf.layer.borderColor = Color.View.SignUp.textFieldBorder
        tf.layer.borderWidth = Size.View.SignUp.textFieldBorderWidth
        
        return tf
    }()
    
    let passwordTextField: MHTextField = {
        let padding = Size.View.SignUp.textFieldInnerPadding
        let height = Size.View.SignUp.textFieldHeight
        
        let tf = MHTextField(padding: padding, height: height)
        tf.placeholder = Const.View.SignUp.passwordPlaceHolder
        tf.isSecureTextEntry = true
        tf.constrainHeight(constant: height)
        
        tf.layer.borderColor = Color.View.SignUp.textFieldBorder
        tf.layer.borderWidth = Size.View.SignUp.textFieldBorderWidth
        return tf
    }()
    
    let btnLogIn: UIButton = {
        let btn = UIButton(type: .system)
        btn.constrainHeight(constant: Size.View.SignUp.btnSignUpHeight)
        btn.layer.cornerRadius = Size.View.SignUp.btnSignUpCornerRadious
        btn.setTitle(Const.View.SignUp.signUp, for: .normal)
        btn.setSystemFont(ofSize: Size.View.SignUp.btnSignUpTitleFont, weight: .heavy)
        btn.backgroundColor = Color.View.SignUp.darkBlue
        btn.setTitleColor(.white, for: .normal)
        return btn
    }()
    
    let labelTermOfUse: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: Size.View.SignUp.txtTermOfUseFont)
        label.attributedText = Const.View.SignUp.lblTermOfUse
        label.textAlignment = .center
        label.isUserInteractionEnabled = true
        label.numberOfLines = 1
        return label
    }()
    
    let textFieldsStackView = UIStackView(axis: .vertical, spacing: Size.View.LogIn.textFieldsStackViewSpacing)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        backgroundColor = .white
        addViews()
        addViewsConstraints()
        setupBtnTermOfUse()
    }
    
    private func addViews() {
        textFieldsStackView.addArrangedSubview(views: [fullNameTextField, phoneNumberTextField ,emailTextField, passwordTextField])
        
        [labelCreateNewAccount, textFieldsStackView, btnLogIn, labelTermOfUse]
            .forEach{ addSubview($0) }
    }
    
    private func addViewsConstraints() {
        let safeArea = safeAreaLayoutGuide
        [labelCreateNewAccount.leftAnchor.constraint(equalTo: leftAnchor, constant: Size.View.SignUp.lblCreateNewAccountLeftPaddign),
         labelCreateNewAccount.rightAnchor.constraint(equalTo: rightAnchor, constant:  Size.View.SignUp.lblCreateNewAccountRightPaddign),
         labelCreateNewAccount.topAnchor.constraint(equalTo: topAnchor, constant:  Size.View.SignUp.lblCreateNewAccountTopPaddign),
         
         textFieldsStackView.leftAnchor.constraint(equalTo: leftAnchor, constant: Size.View.SignUp.textFieldsStackViewLeftPaddign),
         textFieldsStackView.rightAnchor.constraint(equalTo: rightAnchor, constant: Size.View.SignUp.textFieldsStackViewRightPaddign),
         textFieldsStackView.topAnchor.constraint(equalTo: labelCreateNewAccount.bottomAnchor, constant: Size.View.SignUp.textFieldsStackViewTopPadding),
         
         btnLogIn.leftAnchor.constraint(equalTo: leftAnchor, constant: Size.View.SignUp.btnSignUpLeftPadding),
         btnLogIn.rightAnchor.constraint(equalTo: rightAnchor, constant: Size.View.SignUp.btnSignUpRightPadding),
         btnLogIn.topAnchor.constraint(equalTo: textFieldsStackView.bottomAnchor, constant:  Size.View.SignUp.btnSignUpTopPadding),
         
         labelTermOfUse.leftAnchor.constraint(equalTo: leftAnchor),
         labelTermOfUse.rightAnchor.constraint(equalTo: rightAnchor),
         labelTermOfUse.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: Size.View.SignUp.lblTermOfUseBottomPaddign)

         ].forEach{ $0.isActive = true }
    }
    
    private func setupBtnTermOfUse() {
        let labelTermOfUseTapGesture = UITapGestureRecognizer(target: self, action: #selector(labelTermOfUseTapped))
        labelTermOfUse.addGestureRecognizer(labelTermOfUseTapGesture)
    }
    
    // MARK :- objc functions
    @objc private func labelTermOfUseTapped(gesture: UITapGestureRecognizer) {
        print("btn log in tapped!")
    }
}
