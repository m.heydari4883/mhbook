//
//  Color+LogInSignUp.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/12/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

/*************************************************/
/*********** Extension for Controller ************/
/*************************************************/
extension Color.Controller {
    
    struct Welcome {
        static let lightBlue = UIColor(r: 78, g: 116, b: 217)
        /// for btn title color
        static let lightBlack = UIColor(r: 66, g: 70, b: 96)
        /// for label color
        static let darkBlack = UIColor(r: 66, g: 70, b: 96)
        /// border color for btn sign In
        static let btnBorder = UIColor(r: 198, g: 200, b: 211).cgColor

    }
    
    struct SignUp {
        
    }
    
    struct LogIn {
        
    }
}

/**************************************************/
/**************  Extension for View  **************/
/**************************************************/
extension Color.View {
    
    struct SignUp {
        static let lightBlue = UIColor(r: 78, g: 116, b: 217)
        /// border color for text fields
        static let textFieldBorder = UIColor(r: 198, g: 200, b: 211).cgColor
        /// mainly for btn 'Facebook Login' background
        static let darkBlue = UIColor(r: 59, g: 77, b: 137)
    }
    
    struct LogIn {
        static let lightBlue = UIColor(r: 78, g: 116, b: 217)
        /// border color for text fields
        static let textFieldBorder = UIColor(r: 198, g: 200, b: 211).cgColor
        /// mainly for btn 'Facebook Login' background
        static let darkBlue = UIColor(r: 59, g: 77, b: 137)
    }
}
