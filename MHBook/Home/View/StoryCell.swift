//
//  StoryCell.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/14/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

class StoryCell: UICollectionViewCell {
    
    private var isFirstCellInitialization = true
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.text = "Masoud"
        label.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
        label.textColor = .white
        label.textAlignment = .left
        label.numberOfLines = 1
        return label
    }()
    
    let familyLabel: UILabel = {
        let label = UILabel()
        label.text = "Heydari"
        label.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
        label.textColor = .white
        label.textAlignment = .left
        label.numberOfLines = 1
        return label
    }()
    
    let btnImageProfile: StoryButton = {
        let btn = StoryButton(type: .custom)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.padding = 5
        btn.constrainHeight(constant: Size.View.StoryCell.btnImageProfileHeight)
        btn.constrainWidth(constant: Size.View.StoryCell.btnImageProfileWidth)
        btn.setImage(image: #imageLiteral(resourceName: "mas2"))
        btn.layer.cornerRadius = Size.View.StoryCell.btnImageProfileCornerRadius
        btn.layer.borderColor = UIColor.white.cgColor
        btn.layer.borderWidth = 3
        btn.clipsToBounds = true
        btn.layoutIfNeeded()
        return btn
    }()
    
    let imgBackground: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "mas1")
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    private let labelsStackView = UIStackView(axis: .vertical)
    private let gradientLayer = CAGradientLayer()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
        addViews()
        addViewsConstraint()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        backgroundColor = .white
        layer.cornerRadius = 10
        layer.borderWidth = 1
        layer.borderColor = UIColor(white: 0.8, alpha: 0.25).cgColor
        clipsToBounds = true
        
        btnImageProfile.addTarget(self, action: #selector(btnTapped), for: .touchUpInside)
    }
    
    private func addViews() {
        labelsStackView.addArrangedSubview(views: [nameLabel, familyLabel])
        [imgBackground, labelsStackView, btnImageProfile]
            .forEach{ addSubview($0) }
        
        imgBackground.fillToSuperview()
    }
    
    private func addViewsConstraint() {
        [labelsStackView.leftAnchor.constraint(equalTo: leftAnchor, constant: 8),
         labelsStackView.rightAnchor.constraint(equalTo: rightAnchor, constant: -8),
         labelsStackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8),
         
         btnImageProfile.leftAnchor.constraint(equalTo: leftAnchor, constant: Size.View.StoryCell.btnImageProfileLeftPaddign),
         btnImageProfile.topAnchor.constraint(equalTo: topAnchor, constant: Size.View.StoryCell.btnImageProfileTopPaddign),
         
         ].forEach{ $0.isActive = true }
    }
    
    private func updateGradientLayerFrame() {
        gradientLayer.frame = self.frame
    }
    
    override func layoutSublayers(of layer: CALayer) {
        super.layoutSublayers(of: layer)
        setupGradientLayer()
    }
    
    private func setupGradientLayer() {
        if isFirstCellInitialization {

            gradientLayer.frame = bounds
            
            let startColor = UIColor.clear
            let endColor = UIColor.black
            
            let colors = [ startColor, endColor]
            let locations: [CGFloat] = [0.5, 1.5]
            
            let radialGradientLayer = RadialGradientLayer(gradientRadius: 750, colors: [startColor.cgColor, endColor.cgColor])
            radialGradientLayer.frame = imgBackground.bounds
            imgBackground.layer.insertSublayer(radialGradientLayer, above: imgBackground.layer)
            
            imgBackground.addGradient(with: gradientLayer, colorSet: colors, locations: locations)
            isFirstCellInitialization = false
        }
    }
    
    @objc private func btnTapped(_ sender: UIButton) {
        print(" btn story tapped!")
        sender.tappedAnimation()
    }
}


//class RadialGradientLayer: CALayer {
//
//    private var gradientRadius: CGFloat = 0
//    private var colors = [CGColor]()
//
//    init(gradientRadius: CGFloat, colors: [CGColor]) {
//        self.gradientRadius = gradientRadius
//        self.colors = colors
//        super.init()
//        self.needsDisplayOnBoundsChange = true
//
//    }
//
//    required override init() {
//        super.init()
//        needsDisplayOnBoundsChange = true
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//    }
//
//    required override init(layer: Any) {
//        super.init(layer: layer)
//    }
//
//
//    override func draw(in ctx: CGContext) {
//        ctx.saveGState()
//
//        let colorSpace = CGColorSpaceCreateDeviceRGB()
//        let locations: [CGFloat] = [0, 1]
//
//        let gradient = CGGradient(colorsSpace: colorSpace, colors: colors as CFArray, locations: locations)
//
//        let center = CGPoint(x: bounds.width / 2.0, y: bounds.height / 2.0)
//        let option = CGGradientDrawingOptions(rawValue: 0)
//
//        ctx.drawRadialGradient(gradient!, startCenter: center, startRadius: bounds.width / 4.0, endCenter: center, endRadius: gradientRadius, options: option)
//    }
//}
