//
//  HomeController.swift
//  MHBook
//
//  Created by Masoud Heydari on 8/14/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

class HomeController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var feeds = getFeedModels()
    
    // catch image slider off set to prevent it from scrolling
    private var storedOffsets = [Int: CGFloat]()
    
    var layout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        let width = UIScreen.main.bounds.size.width
        layout.sectionInsetReference = .fromSafeArea
        layout.estimatedItemSize = CGSize(width: width, height: 10)
        return layout
    }()
    
    lazy var collectionView: UICollectionView = {
        let cv = UICollectionView(frame: .zero, collectionViewLayout: self.layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = .white
        cv.collectionViewLayout = layout
        return cv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    private func setupView() {
        setupCollectionView()
    }
    
    private func setupCollectionView() {
        collectionView.collectionViewLayout = layout
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = Color.Controller.Home.collectionViewbackground
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.alwaysBounceVertical = true
        
        // register cell
        collectionView.register(HomeFeedCell.self, forCellWithReuseIdentifier: Const.Controller.Home.homeFeedCellId)
        
        // register header
        collectionView.register(HeaderStoryView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: Const.Controller.Home.headerCellId)
        
        view.addSubview(collectionView)
        if #available(iOS 11.0, *) {
            let safeArea = view.safeAreaLayoutGuide
            collectionView.rightAnchor.constraint(equalTo: safeArea.rightAnchor).isActive = true
            collectionView.leftAnchor.constraint(equalTo: safeArea.leftAnchor).isActive = true
            collectionView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        }
    }
    
    private func btnLikeTapped(index: Int) {
        print("btn like tapped!")
        let reloadIndexPath = IndexPath(item: index, section: 0)
        
        // get the current cell to catch the image slider collection view offset
        guard let cell = collectionView.cellForItem(at: reloadIndexPath) as? HomeFeedCell else { return }
        
        // do this for fixing the image slider scrolling issue
        storedOffsets[reloadIndexPath.row] = cell.collectionViewOffset
        
        feeds[index].isLieked = !feeds[index].isLieked
        collectionView.reloadItems(at: [reloadIndexPath])
    }
    
    // img banner selection
    private func imgBannerTapped(index: Int) {
        print("img banner tapped!)")
    }
    
    // descTextView selection
    private func textViewDescriptionTapped(index: Int) {
        print("text view tapped! at index \(index)")
        let reloadIndexPath = IndexPath(item: index, section: 0)
        
        // get the current cell to catch the image slider collection view offset
        guard let cell = collectionView.cellForItem(at: reloadIndexPath) as? HomeFeedCell else { return }
        
        // do this for fixing the image slider scrolling issue
        storedOffsets[reloadIndexPath.row] = cell.collectionViewOffset
        
        // change the mode of text container of descTextView to expand the descTextView
        feeds[index].descriptionTextVeiwTextContainerMode = .byWordWrapping
        
        // deactivate the height anchor of descTextView to be able to expand it's height
        feeds[index].isDescriptionTextViewHeightActive = false
        
        collectionView.reloadData()
    }
}

/***********************************************/
/*************** COLLECTION VIEW ***************/
/***********************************************/
extension HomeController {
    
    // cell will display
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = cell as? HomeFeedCell else { return }
        cell.setImageSliderTage(forItem: indexPath.item)
        cell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
    }
    
    // cell did end display
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = cell as? HomeFeedCell else { return }
        storedOffsets[indexPath.item] = cell.collectionViewOffset
    }
    
    // cell config
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let homeFeedCell = collectionView.dequeueReusableCell(withReuseIdentifier: Const.Controller.Home.homeFeedCellId, for: indexPath) as? HomeFeedCell else { return UICollectionViewCell() }
        
        homeFeedCell.feed = feeds[indexPath.item]
        
        homeFeedCell.btnLikeDidSelect = { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.btnLikeTapped(index: indexPath.item)
        }
        
        homeFeedCell.imgBannerDidSelect = { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.imgBannerTapped(index: indexPath.item)
        }
        
        homeFeedCell.textViewDescriptionDidSelect = { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.textViewDescriptionTapped(index: indexPath.item)
        }
        
        return homeFeedCell
    }
    
    // header view
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: Const.Controller.Home.headerCellId, for: indexPath) as! HeaderStoryView
        return header
    }
    
    // num of cells
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return feeds.count
    }
    
    // collection view transition
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        layout.estimatedItemSize = CGSize(width: view.safeAreaLayoutGuide.layoutFrame.width, height: 10)
        layout.invalidateLayout()
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    // cell size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // calculate cell size
        let dummyCell = HomeFeedCell(frame: .init(x: 0, y: 0, width: view.frame.width, height: 1))
        dummyCell.feed = feeds[indexPath.item]
        
        dummyCell.layoutIfNeeded()
        
        let estimatedHeight = dummyCell.systemLayoutSizeFitting(.init(width: view.frame.width, height: 1)).height
        
        return .init(w: view.frame.width, h: estimatedHeight)
    }
    
    // header height
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return Size.Controller.Home.storyHeaderViewSize
    }
    
    // gape size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
}

